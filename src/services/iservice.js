import HttpClient from '../helpers/http.client';

class IService {
    constructor () {
        this.baseUrl = 'https://venados.dacodes.mx/api';
        this.http = new HttpClient({ baseURL: this.baseURL });
    }
}

export default IService;