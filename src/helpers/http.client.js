import axios from 'axios';

class HttpClient {
    constructor (options = {}) {
        this.instance = axios.create(options);
    }

    async get(apiPath, headers = {}) {
        headers[ 'Accept' ] = 'application/json';
        return this.instance.get(apiPath, { headers });
    }
}

export default HttpClient;