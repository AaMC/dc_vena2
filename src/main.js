import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

import App from './App.vue';
import router from './router';
import store from './store';
import config from './config';

import SoccerIcon from 'vue-material-design-icons/Soccer.vue';
import CalendarMonthIcon from 'vue-material-design-icons/CalendarMonth.vue';
import CounterIcon from 'vue-material-design-icons/Counter.vue';
import AccountGroupIcon from 'vue-material-design-icons/AccountGroup.vue';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'vue-material-design-icons/styles.css';

Vue.config.productionTip = false;

Vue.use(BootstrapVue);

Vue.component('soccer-icon', SoccerIcon);
Vue.component('calendar-month-icon', CalendarMonthIcon);
Vue.component('counter-icon', CounterIcon);
Vue.component('account-group-icon', AccountGroupIcon);

global.config = config.dev;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
