import Vue from 'vue';
import Router from 'vue-router';

import Home from './views/Home.vue';
import Layout from './views/Layout';
import error404 from './views/error404';
import Games from './views/Games';
import Stats from './views/Stats';
import Team from './views/Team';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Layout,
      children: [
        {
          path: 'games',
          name: 'games',
          component: Games
        },
        {
          path: 'stats',
          name: 'stats',
          component: Stats
        },
        {
          path: 'team',
          name: 'team',
          component: Team
        },
      ]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '*',
      name: 'notfound',
      component: error404
    }
  ]
})
